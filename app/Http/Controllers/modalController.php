<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\stories;

class modalController extends Controller
{
     public function Viewcandidates($id,Request $request){
        $candidates = stories::findOrFail($id);
        return view('validator.review.modal.view_candidates')->with('candidates',$candidates);
    }

    public function ViewPendingCandidates($id,Request $request){
        $candidates = stories::findOrFail($id);
        return view('validator.review.modal.view_candidates')->with('candidates',$candidates);
    }

    public function deletestories(Request $request){
        

        $rejected = stories::findOrFail($request->id);
        $rejected->delete();


         if ($rejected->save()) {
            return redirect()->back();
         }else{
            return redirect()->back();
         }
    }

     public function approvestories(Request $request){
        

        $acceptstories              = stories::where('id',$request->id)->update([
        'status'                    => 'P',
       ]);

         if ($acceptstories) {
            return redirect()->back();
         }else{
            return redirect()->back();
         }
    }

    public function approvePendingStories(Request $request){
        

        $acceptstories              = stories::where('id',$request->id)->update([
        'status'                    => 'A',
       ]);

         if ($acceptstories) {
            return redirect()->back();
         }else{
            return redirect()->back();
         }
    }
}
