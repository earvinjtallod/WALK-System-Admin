<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\stories;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('home');
    }
    public function review(Request $request)
    {
        
        $forreview = stories::where('status','For review')->get();
        return view('validator.review.tbl_forreview')->with('forreview', $forreview);
    }

    
    public function pendingStories(Request $request)
    {
        
        $pending = stories::where('status','P')->get();
        return view('validator.review.tbl_forpending')->with('pending', $pending);
    }

     public function AcceptPending(Request $request)
    {
        
        $Acceptpending = stories::where('status','P')->get();
        return view('validator.review.tbl_acceptPending')->with('Acceptpending', $Acceptpending);
    }

    public function ApprovedRequest(Request $request)
    {
        
        $ApprovedRequest = stories::where('status','A')->get();
        return view('validator.review.tbl_approvedrequest')->with('ApprovedRequest', $ApprovedRequest);
    }
}
