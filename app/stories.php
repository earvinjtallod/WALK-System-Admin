<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stories extends Model
{
     protected $table ='stories';
      protected $fillable = [
        'id',
       'name',
       'amount',
       'email',
       'age',
       'gender',
       'address',
       'story',
       'mobile',
       'uploadedby',
       'status'
    ];
    public $incrementing = false;
    public $timestamps = false;


    
}
