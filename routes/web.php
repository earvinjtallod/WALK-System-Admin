<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/table-for-stories', 'HomeController@review')->name('review');
Route::get('/table-for-pending', 'HomeController@pendingStories')->name('pendingStories');



/*for modal*/
Route::get('/view-candidates/{id}', 'modalController@Viewcandidates');
Route::get('/uploads/reject/post/', 'modalController@Viewcandidates');/*ajax*/
Route::post('/decline-candidates', 'modalController@deletestories');
Route::post('/approve-candidates', 'modalController@approvestories')->name('approve-candidates');
/*admin*/
Route::get('/view-pending-candidates/{id}', 'modalController@ViewPendingCandidates');
Route::post('/approve-pending-candidates', 'modalController@approvePendingStories')->name('approve-pending-candidates');
Route::get('/table-for-approved-request', 'HomeController@AcceptPending')->name('AcceptPending');
Route::get('/table-for-approvedrequest', 'HomeController@ApprovedRequest')->name('approvedrequest');