<div class="card">
        <div class="card-body">
            <h4 class="card-title">Review Stories</h4>
            <div class="table-responsive">
                <table class="table color-table warning-table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>Amount Needed</th>
                            <th>age</th>
                            <th>gender</th>
                            <th>address</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach($ApprovedRequest as $ApprovedRequests)
                        <tr>
                            <td>{{ $ApprovedRequests->id }}</td>
                            <td>{{ $ApprovedRequests->name }}</td>
                            <td>{{ $ApprovedRequests->amount }}</td>
                            <td>{{ $ApprovedRequests->age }}</td>
                            <td>{{ $ApprovedRequests->gender }}</td>
                            <td>{{ $ApprovedRequests->address }}</td>
                       
                            <td>

                            <button class="btn btn-default btn-icon add-tooltip" data-target="#PendingCandidates" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="View Gateway ID" onclick="AcceptPending({{{$ApprovedRequests->id}}})">
                            <i class="ion-eye icon-sm"></i>
                            </button>
                            
                      </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>