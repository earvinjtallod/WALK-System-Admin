<div class="card">
        <div class="card-body">
            <h4 class="card-title">Review Stories</h4>
            <div class="table-responsive">
                <table class="table color-table warning-table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>name</th>
                            <th>Amount Needed</th>
                            <th>age</th>
                            <th>gender</th>
                            <th>address</th>
                            <th>action</th>
                        </tr>
                    </thead>
                    <tbody>
                         @foreach($pending as $pendings)
                        <tr>
                            <td>{{ $pendings->id }}</td>
                            <td>{{ $pendings->name }}</td>
                            <td>{{ $pendings->amount }}</td>
                            <td>{{ $pendings->age }}</td>
                            <td>{{ $pendings->gender }}</td>
                            <td>{{ $pendings->address }}</td>
                       
                            <td>
                                <button class="btn btn-default btn-icon add-tooltip" data-target="#candidates" data-toggle="modal" data-placement="top" data-toggle="tooltip" data-original-title="View Gateway ID" onclick="seecandidates({{{$pendings->id}}})">
                            <i class="ion-eye icon-sm"></i>
                            </button>
              
                            
                      </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>